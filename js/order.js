/**
 * Search.js 
 * 
 * @Darryl
 * 
 */
$(document).ready(function() {
	$('#article').keyup(function() {
		$.ajax({
			type : "POST",
			url : "ajax/supply.php",
			data : "action=search&search_value=" + $('#search_value').val(),
			success : function(data) {
				$('#value').html(data);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
	});
});
