<?php
/**
 * Article.php - renders the main page
 *
 * @author Bugslayer
 *
 */
?>
<div class="jumbotron">
	<h1>Welkom op onze site</h1>
	<p>
		Logisch dat je bij ons terecht bent gekomen. Wij staan bovenaan elke
		zoekopdracht in Google.
	</p>
	<table class="jumbotron table-striped">
	<table border="1px">
		<thead>

			<th> Browser type </th>
			<th> Statistieken </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>IE (<=11)</td>
				<td>40,6%</td>
			</tr>
			<tr>
				<th>Chrome </th>
				<td>24,2%</td>
			</tr>
			<tr>
				<td>Safari</td>
				<td>24,2%</td>
			</tr>
			<tr>
				<td>Firefox</td>
				<td>10,4%</td>
			</tr>
			<tr>
				<td><i>Android</i></td>
				<td>3,0%</td>
			</tr>
			<tr>
				<td>Opera</td>
				<td>0,6%</td>
			</tr>
			<tr>
				<td>Overig (inc. mobiel) </td>
				<td>0,6%</td>
			</tr>

	</table>
<h2>Ons hoofdkantoor</h2>
<img src="./img/head-office.png">
</div>