<?php
/**
 * Order.php - renders an order form where the authenticated user can place an order
 * 
 * @author Bugslayer
 * 
 */
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h1>Bestelformulier</h1>
		<p>Gebruik dit formulier om uw Slevels, wigbekken en zwenkmoeren bij
		ons te bestellen</p>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" name="input"
			 action="?action=save&page=order"  method="post">
			<input type="hidden" name="send" value="true" />
			<div class="form-group">
				<label class="control-label col-sm-2" for="slevels">Aantal slevels</label>
				<div class="col-sm-2">
					<input class="form-control" type="number" name="slevels" value="0">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="wigbekken">Aantal wigbekken</label>
				<div class="col-sm-2">
					<input class="form-control" type="number" name="wigbekken" value="0">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="zwenkmoeren">Aantal zwenkmoeren</label>
				<div class="col-sm-2">
					<input class="form-control" type="number" name="Zwenkmoeren" value="0">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Verstuur</button>
				</div>
			</div>
		</form>
	</div>
</div>

<?php
// Render the quantity.
if ($checkvalue) {
	$result = check_value ( $searchCmd );
	if (sizeof ( $result ) > 0) {
		echo "<ul>";
		foreach ( $result as $row ) {
			echo "<li>";
			echo '<b><a href="?action=show&page=article&id=' . $row ['Content'];
			echo "</li>";
		}
		echo "</ul>";
	} else {
		echo "Het artikel is niet op voorraad.";
	}
}
?>
