<?php
/**
 * Contact.php - renders a contact form
 *
 * @author Bugslayer
 *
 */
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h1>Contact</h1>
		<p>
			Vul dit formulier in om een bericht te sturen:
		</p>
	</div>
	<div class="panel-body">
		<form id="contactform" class="form-horizontal" name="input"
		action="?action=save&page=contact" method="post">
			<div class="form-group">
				<label class="control-label col-sm-2" for="Name_first">Voornaam *</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Name_first" maxlength="50"
					size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Name_middle">Tussenvoegsel</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Name_middle" maxlength="50"
					size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Name_last">Achternaam *</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Name_last" maxlength="50"
					size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Email Adres *</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="email" maxlength="50"
					size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Telephone">Telefoonnummer</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Telephone" maxlength="50"
					size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Subject">Onderwerp *</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Subject" maxlength="50"
					size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Message">Bericht *</label>
				<div class="col-sm-10">
					<textarea class="form-control" name="Message" maxlength="50"
						rows="10"></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">
						Verstuur
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script src="js/contact.js"></script>
